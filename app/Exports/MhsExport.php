<?php

namespace App\Exports;

use App\Models\TbMhs;
use Maatwebsite\Excel\Concerns\FromCollection;

class MhsExport implements FromCollection
{
  /**
   * @return \Illuminate\Support\Collection
   */
  public function collection()
  {
    return TbMhs::all();
  }
}
