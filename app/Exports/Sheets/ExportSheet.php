<?php

namespace App\Exports\Sheets;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ExportSheet implements FromView, ShouldAutoSize
{
  public function __construct($view_path, $data_excel, $title)
  {
    $this->view_path = $view_path;
    $this->data_excel = [
      'data_excel' => $data_excel,
      'title' => $title
    ];
  }

  public function view(): View
  {
    return view($this->view_path, $this->data_excel);
  }
}
