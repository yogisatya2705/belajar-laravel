<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ExportExcel implements FromView, ShouldAutoSize
{
  public function __construct($data_sheet)
  {
    $this->view_path = $data_sheet['view_path'];
    $this->data_excel = [
      'data_excel' => $data_sheet['data_excel'],
      'title' => $data_sheet['title']
    ];
  }

  public function view(): View
  {
    return view($this->view_path, $this->data_excel);
  }
}
