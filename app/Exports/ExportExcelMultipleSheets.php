<?php

namespace App\Exports;

use App\Exports\Sheets\ExportSheet;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;

class ExportExcelMultipleSheets implements WithMultipleSheets
{
  public function __construct($data_sheet)
  {
    $this->data_sheet = $data_sheet;
  }

  public function sheets(): array
  {
    $sheets = [];

    foreach ($this->data_sheet as $key => $value) {
      $sheets[] = new ExportSheet($value['view_path'], $value['data_excel'], $value['title']);
    }

    return $sheets;
  }
}
