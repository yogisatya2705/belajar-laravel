<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
  public function getLogin()
  {
    return view('pages.auth.login');
  }

  public function postLogin(Request $request)
  {
    $this->validate($request, [
      'username' => 'required',
      'password' => 'required'
    ]);

    if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
      return redirect()->route('login');
    }

    return redirect()->route('login')->with(['error' => 'Username dan password tidak cocok']);
  }

  public function logout()
  {
    if (Auth::check()) {
      Auth::logout();
    }

    return redirect('/');
  }
}
