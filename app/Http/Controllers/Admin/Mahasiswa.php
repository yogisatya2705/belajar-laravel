<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ExportExcel;
use App\Exports\ExportExcelMultipleSheets;
use App\Exports\MhsExport;
use App\Http\Controllers\Controller;
use App\Models\TbMhs;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class Mahasiswa extends Controller
{
  private $route_root = 'mahasiswa.';

  public function prepare_index($data = [])
  {
    return array_merge($data, [
      'sidebar' => 'mahasiswa'
    ]);
  }

  // READ
  public function read()
  {
    return view('pages.mahasiswa.read', $this->prepare_index());
  }

  // CREATE
  public function create()
  {
    return view('pages.mahasiswa.create', $this->prepare_index());
  }
  public function create_post(Request $request)
  {
    $validated_data = $request->validate([
      'nim'  => ['required', 'numeric', 'digits_between:1,20', 'unique:tb_mhs,nim'],
      'nama' => ['required', 'string', 'max:255'],
      'jk'   => ['required', 'string', 'max:1', 'in:L,P'],
    ]);

    try {
      TbMhs::create([
        'nim'  => $request['nim'],
        'nama' => $request['nama'],
        'jk'   => $request['jk'],
      ]);

      return redirect()->route($this->route_root . 'read')->with('toastr_succ', 'Simpan data sukses');
    } catch (\Throwable $th) {
      return back()->with('toastr_erro', 'Terjadi kesalahan saat menyimpan data');
    }
  }

  // UPDATE
  public function update($id)
  {
    $data = [
      'data_edit' => TbMhs::find($id)
    ];

    if (!$data['data_edit']) {
      return back();
    }

    return view('pages.mahasiswa.update', $this->prepare_index($data));
  }
  public function update_post(Request $request)
  {
    $validated_data = $request->validate([
      'nim'  => ['required', 'numeric', 'digits_between:1,20', 'unique:tb_mhs,nim,' . $request['id']],
      'nama' => ['required', 'string', 'max:255'],
      'jk'   => ['required', 'string', 'max:1', 'in:L,P'],
    ]);

    try {
      $data_edit = TbMhs::find($request['id']);
      if (!$data_edit) {
        return back()->with('toastr_erro', 'Data tidak ditemukan');
      }

      $data_edit['nim']  = $request['nim'];
      $data_edit['nama'] = $request['nama'];
      $data_edit['jk']   = $request['jk'];
      $data_edit->save();

      return redirect()->route($this->route_root . 'read')->with('toastr_succ', 'Update data sukses');
    } catch (\Throwable $th) {
      return back()->with('toastr_erro', 'Terjadi kesalahan saat memperbaharui data');
    }
  }

  // DELETE
  public function delete_post(Request $request)
  {
    try {
      if (TbMhs::destroy($request['id'])) {
        return redirect()->route($this->route_root . 'read')->with('toastr_succ', 'Hapus data sukses');
      }

      return back()->with('toastr_erro', 'Data masih digunakan pada fungsional lainnya');
    } catch (\Throwable $th) {
      return back()->with('toastr_erro', 'Terjadi kesalahan saat menghapus data');
    }
  }

  // DATATABLE
  public function datatable()
  {
    $data = TbMhs::select(
      'tb_mhs.id',
      'tb_mhs.nim',
      'tb_mhs.nama',
      'tb_mhs.jk',
      'tb_mhs.created_at',
      'tb_mhs.updated_at',
    );

    return DataTables::of($data)
      ->addColumn('created_at', function ($data) {
        return date("d/m/Y H:i:s", strtotime($data->created_at));
      })
      ->addColumn('updated_at', function ($data) {
        return date("d/m/Y H:i:s", strtotime($data->updated_at));
      })
      ->addColumn('jk', function ($data) {
        return $data->jk == "L" ? "Laki - laki" : ($data->jk == "P" ? "Perempuan" : "Other");
      })
      ->addColumn('action', function ($data) {
        return '
          <a class="btn btn-sm btn-primary btn-icon-split" href="' . route('mahasiswa.update', ['id' => $data->id]) . '">
            <span class="icon text-white-50">
              <i class="fas fa-pencil-alt"></i>
            </span>
            <span class="text">Edit</span>
          </a>

          <button class="btn btn-sm btn-danger btn-icon-split" onclick="toggleModalDelete(\'data-table-del-info-' . $data->id . '\', \'' . $data->id . '\')">
            <span class="icon text-white-50">
                <i class="fas fa-trash"></i>
            </span>
            <span class="text">Hapus</span>
          </button>
          <span id="data-table-del-info-' . $data->id . '" class="d-none">' . $data->nim . ' - ' . $data->nama . '</span>
        ';
      })
      ->addIndexColumn()
      ->escapeColumns([])
      ->make(true);
  }

  public function export_all($tipe)
  {
    if ($tipe == "excel") {
      // return Excel::download(new MhsExport, 'Data Mahasiswa.xlsx', \Maatwebsite\Excel\Excel::XLSX);

      // return Excel::download(new ExportExcel(
      //   [
      //     'view_path'  => 'exports.mhs',
      //     'data_excel' => TbMhs::all(),
      //     'title'      => 'LEMBAR1',
      //   ]
      // ), 'Data Mahasiswa.xlsx', \Maatwebsite\Excel\Excel::XLSX);

      return Excel::download(new ExportExcelMultipleSheets(
        [
          [
            'view_path'  => 'exports.mhs',
            'data_excel' => TbMhs::all(),
            'title'      => 'LEMBAR1',
          ],
          [
            'view_path'  => 'exports.mhs',
            'data_excel' => TbMhs::all(),
            'title'      => 'LEMBAR2',
          ],
        ]
      ), 'Data Mahasiswa.xlsx', \Maatwebsite\Excel\Excel::XLSX);
    }

    if ($tipe == "pdf") {
      // return Excel::download(new MhsExport, 'Data Mahasiswa.pdf', \Maatwebsite\Excel\Excel::MPDF);

      return Excel::download(new ExportExcel(
        [
          'view_path'  => 'exports.mhs',
          'data_excel' => TbMhs::all(),
          'title'      => 'LEMBAR1',
        ]
      ), 'Data Mahasiswa.pdf', \Maatwebsite\Excel\Excel::MPDF);
    }

    abort(404);
  }
}
