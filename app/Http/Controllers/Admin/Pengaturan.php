<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class Pengaturan extends Controller
{
  private $route_root = 'pengaturan.';

  public function prepare_index($data = [])
  {
    return array_merge([
      'sidebar' => 'pengaturan.deskripsi-sistem'
    ], $data);
  }

  public function get_deskripsi_sistem()
  {
    return view('pages.sys_desc.update', $this->prepare_index([
      'data_edit' => json_decode(file_get_contents(storage_path('json/sysdesc.json')), true)
    ]));
  }

  public function post_deskripsi_sistem(Request $request)
  {
    $validated_data = $request->validate([
      'name'    => ['required', 'string', 'max:255'],
      'company' => ['required', 'string', 'max:255'],
      'address' => ['required', 'string', 'max:255'],
      'version' => ['required', 'string', 'max:255'],
    ]);

    try {
      $data_edit = json_decode(file_get_contents(storage_path('json/sysdesc.json')), true);

      $data_edit['name']    = $request['name'];
      $data_edit['company'] = $request['company'];
      $data_edit['address'] = $request['address'];
      $data_edit['version'] = $request['version'];

      if ($request->file('logo')) {
        try {
          unlink(public_path($data_edit['logo']));

          $title = Str::random(10);
          $request->file('logo')->move(public_path(),  $title . "." . $request->file('logo')->getClientOriginalExtension());

          $data_edit['logo'] = $title . "." . $request->file('logo')->getClientOriginalExtension();
        } catch (\Throwable $th) {
          throw $th;
        }
      }

      file_put_contents(storage_path('json/sysdesc.json'), json_encode($data_edit, JSON_PRETTY_PRINT));

      return redirect()->route($this->route_root . 'deskripsi-sistem')->with('toastr_succ', 'Update data sukses');
    } catch (\Throwable $th) {
      return back()->with('toastr_erro', 'Terjadi kesalahan saat memperbaharui data');
    }
  }
}
