<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TbMhs extends Model
{
  use HasFactory;

  /** @var string  */
  protected $table = 'tb_mhs';

  /** @var array  */
  protected $guarded = [];
}
