<?php

namespace App\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot()
  {
    date_default_timezone_set('Asia/Makassar');

    $data = file_get_contents(storage_path('json/sysdesc.json'));
    $data = json_decode($data, true);

    Config::set('sys.name',    $data ? $data['name'] : '');
    Config::set('sys.company', $data ? $data['company'] : '');
    Config::set('sys.address', $data ? $data['address'] : '');
    Config::set('sys.version', $data ? $data['version'] : '');
    Config::set('sys.logo',    $data ? '//public/' . $data['logo'] : '');
  }
}
