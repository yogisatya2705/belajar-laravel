<?php

use App\Http\Controllers\Admin\Mahasiswa;
use App\Http\Controllers\Admin\Pengaturan;
use App\Http\Controllers\Auth\LoginController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//   echo "Hello world";
// });
Route::get('/', function () {
  return redirect()->route('mahasiswa.read');
});


// ROUTE AUTH
Route::get('/login', [LoginController::class, 'getLogin'])->middleware('guest')->name('login');
Route::post('/login', [LoginController::class, 'postLogin']);
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');


// ROUTE PENGATURAN
Route::prefix('pengaturan')->middleware('auth')->name('pengaturan.')->group(function () {
  Route::get('/', function () {
    return redirect()->route('pengaturan.deskripsi-sistem');
  });

  Route::prefix('deskripsi-sistem')->group(function () {
    Route::get('/', [Pengaturan::class, 'get_deskripsi_sistem'])->name('deskripsi-sistem');
    Route::post('/', [Pengaturan::class, 'post_deskripsi_sistem']);
  });
});

// ROUTE MAHASISWA
Route::prefix('mahasiswa')->middleware('auth')->name('mahasiswa.')->group(function () {
  Route::get('/', [Mahasiswa::class, 'read'])->name('read');

  Route::get('create', [Mahasiswa::class, 'create'])->name('create');
  Route::post('create', [Mahasiswa::class, 'create_post']);

  Route::get('update/{id}', [Mahasiswa::class, 'update'])->where(['id' => '[0-9]+'])->name('update');
  Route::post('update/{id}', [Mahasiswa::class, 'update_post'])->where(['id' => '[0-9]+']);

  Route::post('delete', [Mahasiswa::class, 'delete_post'])->name('delete');

  Route::get('datatable', [Mahasiswa::class, 'datatable'])->name('datatable');

  Route::get('export-all/{tipe}', [Mahasiswa::class, 'export_all'])->name('export-all');
});
