<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>{{$title}}</title>
</head>
<body>
  <table>
    <tr></tr>
    <tr>
      <td valign="center" align="center" style="border-top: 1px solid black;border-bottom: 1px solid black;border-left: 1px solid black;"><b>NO</b></td>
      <td valign="center" align="center" style="border-top: 1px solid black;border-bottom: 1px solid black;border-left: 1px solid black;"><b>NIM</b></td>
      <td valign="center" align="center" style="border-top: 1px solid black;border-bottom: 1px solid black;border-left: 1px solid black;"><b>NAMA</b></td>
      <td valign="center" align="center" style="border-top: 1px solid black;border-bottom: 1px solid black;border-left: 1px solid black;border-right: 1px solid black;"><b>JENIS KELAMIN</b></td>
    </tr>

    @foreach ($data_excel as $item)
      <tr>
        <td valign="center" align="center" style="border-bottom: 1px solid black;border-left: 1px solid black;">{{$loop->iteration}}</td>
        <td valign="center" align="center" style="border-bottom: 1px solid black;border-left: 1px solid black;">{{$item->nim}}</td>
        <td valign="center" align="left"   style="border-bottom: 1px solid black;border-left: 1px solid black;">{{$item->nama}}</td>
        <td valign="center" align="center" style="border-bottom: 1px solid black;border-left: 1px solid black;border-right: 1px solid black;">{{$item->jk}}</td>
      </tr>
    @endforeach
  </table>
</body>
</html>