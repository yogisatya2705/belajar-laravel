<footer class="sticky-footer bg-white">
  <div class="container my-auto">
    <div class="copyright text-center my-auto">
      {{-- <span>Copyright &copy; Your Website 2020</span> --}}
      <span>Copyright &copy; {{Config::get('sys.company')}} • {{date("Y")}}</span>
    </div>
  </div>
</footer>