<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

  <!-- Sidebar - Brand -->
  <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{URL('/')}}">
    <div class="sidebar-brand-icon">
      <img src="{{url('/')}}/{{Config::get('sys.logo')}}" alt="" style="height: 50px; width: auto;">
    </div>
    <div class="sidebar-brand-text mx-3">{{Config::get('sys.name')}}</div>
  </a>

  <!-- Divider -->
  <hr class="sidebar-divider my-0">







  <!-- Nav Item - Data Mahasiswa -->
  <li class="nav-item {{$sidebar == "mahasiswa" ? "active" : ""}}">
    <a class="nav-link" href="{{route('mahasiswa.read')}}">
      <i class="fas fa-fw fa-tachometer-alt"></i>
      <span>Data Mahasiswa</span></a>
  </li>

  <!-- Nav Item - Identitas Sistem -->
  <li class="nav-item {{$sidebar == "pengaturan.deskripsi-sistem" ? "active" : ""}}">
    <a class="nav-link" href="{{route('pengaturan.deskripsi-sistem')}}">
      <i class="fas fa-fw fa-wrench"></i>
      <span>Identitas Sistem</span></a>
  </li>














  <!-- Divider -->
  <hr class="sidebar-divider d-none d-md-block">

  <!-- Sidebar Toggler (Sidebar) -->
  <div class="text-center d-none d-md-inline">
    <button class="rounded-circle border-0" id="sidebarToggle"></button>
  </div>

</ul>