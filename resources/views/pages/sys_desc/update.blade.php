@extends('template.layout')

@section('title')
Identitas Sistem
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <form method="POST" action="{{route('pengaturan.deskripsi-sistem')}}" enctype="multipart/form-data">
      @csrf
      <div class="card mb-4">
        <div class="card-header">
          Identitas Sistem
        </div>
        <div class="card-body">
          <div class="form-group">
            <label for="name">Nama Sistem</label>
            <input id="name" name="name" type="text" class="form-control @if($errors->has('name')) is-invalid @endif" @if(!empty($errors->all())) value="{{old('name')}}" @else value="{{$data_edit['name']}}" @endif>
            <div id="name-error" class="invalid-feedback">
              {{$errors->first('name')}}
            </div>
          </div>

          <div class="form-group">
            <label for="company">Nama Perusahaan</label>
            <input id="company" name="company" type="text" class="form-control @if($errors->has('company')) is-invalid @endif" @if(!empty($errors->all())) value="{{old('company')}}" @else value="{{$data_edit['company']}}" @endif>
            <div id="company-error" class="invalid-feedback">
              {{$errors->first('company')}}
            </div>
          </div>

          <div class="form-group">
            <label for="address">Alamat</label>
            <input id="address" name="address" type="text" class="form-control @if($errors->has('address')) is-invalid @endif" @if(!empty($errors->all())) value="{{old('address')}}" @else value="{{$data_edit['address']}}" @endif>
            <div id="address-error" class="invalid-feedback">
              {{$errors->first('address')}}
            </div>
          </div>

          <div class="form-group">
            <label for="version">Versi Sistem</label>
            <input id="version" name="version" type="text" class="form-control @if($errors->has('version')) is-invalid @endif" @if(!empty($errors->all())) value="{{old('version')}}" @else value="{{$data_edit['version']}}" @endif>
            <div id="version-error" class="invalid-feedback">
              {{$errors->first('version')}}
            </div>
          </div>

          <div class="form-group">
            <label for="logo">Logo Sistem</label>
            <input id="logo" name="logo" type="file" accept="image/*" class="form-control @if($errors->has('logo')) is-invalid @endif">
            <div class="text-muted">* Isikan jika ingin mengubah logo</div>
            <div id="logo-error" class="invalid-feedback">
              {{$errors->first('logo')}}
            </div>
          </div>

        </div>
        <div class="card-footer text-muted">
          <button type="submit" class="btn btn-primary float-right">SIMPAN PERUBAHAN</button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection

@section('css')

@endsection

@section('js')

@endsection