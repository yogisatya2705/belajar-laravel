@extends('template.layout')

@section('title')
Tambah Data Mahasiswa
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <form method="POST" action="{{route('mahasiswa.create')}}" enctype="multipart/form-data">
      @csrf
      <div class="card mb-4">
        <div class="card-header">
          Tambah Data Mahasiswa
        </div>
        <div class="card-body">
          <div class="form-group">
            <label for="nim">NIM</label>
            <input id="nim" name="nim" type="text" class="form-control @if($errors->has('nim')) is-invalid @endif" @if(!empty($errors->all())) value="{{old('nim')}}" @else @endif>
            <div id="nim-error" class="invalid-feedback">
              {{$errors->first('nim')}}
            </div>
          </div>

          <div class="form-group">
            <label for="nama">Nama</label>
            <input id="nama" name="nama" type="text" class="form-control @if($errors->has('nama')) is-invalid @endif" @if(!empty($errors->all())) value="{{old('nama')}}" @else @endif>
            <div id="nama-error" class="invalid-feedback">
              {{$errors->first('nama')}}
            </div>
          </div>


          <div class="form-group">
            <label for="nama">Jenis Kelamin</label>

            <div class="form-check">
              <input type="radio" name="jk" value="L" class="form-check-input" id="jk-l">
              <label class="form-check-label" for="jk-l">Laki - laki</label>

              <input type="radio" name="jk" value="P" class="form-check-input" id="jk-p" style="margin-left: 1.25rem">
              <label class="form-check-label" for="jk-p" style="margin-left: 2.5rem">Perempuan</label>
            </div>
            <small id="nama-error" class="text-danger">
              {{$errors->first('jk')}}
            </small>
          </div>
        </div>
        <div class="card-footer text-muted">
          <button type="submit" class="btn btn-primary float-right">SIMPAN</button>
        </div>
      </div>
    </form>
  </div>
</div>
@endsection

@section('css')
@endsection

@section('js')
@endsection