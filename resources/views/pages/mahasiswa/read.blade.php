@extends('template.layout')

@section('title')
Data Mahasiswa
@endsection

@section('content')
<div class="row">
  <div class="col-12">
    <div class="card mb-4">
      <div class="card-header">
        Data Mahasiswa

        <a href="{{route('mahasiswa.create')}}" class="btn btn-sm btn-primary float-right">Tambah</a>
        <a href="{{route('mahasiswa.export-all', ['tipe' => "excel"])}}" class="btn btn-sm btn-success float-right mr-2">Export Excel</a>
        <a href="{{route('mahasiswa.export-all', ['tipe' => "pdf"])}}" class="btn btn-sm btn-danger float-right mr-2">Export PDF</a>
      </div>
      <div class="card-body">

        <div class="table-responsive">
          <table class="table table-bordered" id="data-table" width="100%" cellspacing="0">
            <thead>
              <tr>
                <th>#</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Terakhir Diperbaharui</th>
                <th>Action</th>
              </tr>
            </thead>
            <tfoot>
              <tr>
                <th>#</th>
                <th>NIM</th>
                <th>Nama</th>
                <th>Jenis Kelamin</th>
                <th>Terakhir Diperbaharui</th>
                <th>Action</th>
              </tr>
            </tfoot>
            <tbody>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal Delete -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="modalDeleteLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalDeleteLabel">Konfirmasi Hapus Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Hapus data <strong><i><span id="msg-delete"></span></i></strong> ?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
        <form action="{{route('mahasiswa.delete')}}" method="post">
          @csrf
          <input type="hidden" name="id" value="">
          <button type="submit" class="btn btn-danger">Hapus Data</button>
        </form>
      </div>
    </div>
  </div>
</div>
@endsection

@section('css')
<link href="{{url('/public')}}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endsection

@section('js')
<script src="{{url('/public')}}/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="{{url('/public')}}/vendor/datatables/dataTables.bootstrap4.min.js"></script>

<script>
  $(document).ready(function() {
    $('#data-table').dataTable({
      processing: true,
      serverSide: true,
      ajax: "{{route('mahasiswa.datatable')}}",
      columns: [{
          data: 'DT_RowIndex',
          name: 'DT_RowIndex',
          render: null,
          orderable: false,
          searchable: false,
          exportable: false,
          printable: true,
          footer: '',
        },
        {
          data: 'nim',
          name: 'nim'
        },
        {
          data: 'nama',
          name: 'nama'
        },
        {
          data: 'jk',
          name: 'jk'
        },
        {
          data: 'updated_at',
          name: 'updated_at'
        },
        {
          data: 'action',
          name: 'action'
        },
      ]
    });
  });
</script>

{{-- MODAL DELETE --}}
<script>
  function toggleModalDelete(id_msg, id) {
    $("#msg-delete").html($("#" + id_msg).text())
    $("input[name='id']").val(id)
    $('#modalDelete').modal('toggle')
  }
</script>
@endsection