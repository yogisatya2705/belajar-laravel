@extends('template.layout_auth')

@section('title')
Login
@endsection

@section('content')
<div class="row justify-content-center">

  <div class="col-xl-10 col-lg-12 col-md-9">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row justify-content-center">
          <div class="col-lg-6">

            @if ($message = Session::get('error'))
            <div class="pt-5 px-5">
              <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <strong>{{$message}}</strong>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            </div>
            @endif

            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">Login</h1>
              </div>
              <form class="user" method="POST" action="{{route('login')}}" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                  <input id="username" name="username" type="text" placeholder="Username" class="form-control form-control-user @if($errors->has('username')) is-invalid @endif" @if(!empty($errors->all())) value="{{old('username')}}" @else @endif>
                  <div id="username-error" class="invalid-feedback">
                    {{$errors->first('username')}}
                  </div>
                </div>
                <div class="form-group">
                  <input id="password" name="password" type="password" placeholder="Password" class="form-control form-control-user @if($errors->has('password')) is-invalid @endif">
                  <div id="password-error" class="invalid-feedback">
                    {{$errors->first('password')}}
                  </div>
                </div>
                <button type="submit" class="btn btn-primary btn-user btn-block">Login</button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

</div>
@endsection

@section('css')

@endsection

@section('js')

@endsection